@extends('layouts.app')

@section('content')

    @if(isset($comentarios) && count($comentarios) > 0)
        @foreach($comentarios as $comentario)
            <table class="container">
                @if($comentario->user_id === auth()->user()->id)
                    <tr>
                        @if(auth()->user()->role == 0 && $comentario->user_id != auth()->user()->id)
                            <td>
                                <div class="row">
                                <div class="col">
                                    de  {{App\Models\User::findOrFail($comentario->user_id)->name}}:
                                </div>
                            </td>
                        @endif
                        <td class="content">
                            <div class="row">
                                <div class="col">
                                    en  {{App\Models\Musica::findOrFail($comentario->musica_id)->title}}:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mb-3">
                                    <h1 class="text.wrap">{{$comentario->content}}</h1>
                                </div>
                            </div>
                        </td>
                        @if(!Auth::guest() && (auth()->user()->role == 0 || auth()->user()->id == $comentario->user_id))
                            <td align="right">
                                <form method="POST" action="{{route('comentarios.destroy',['id'=>$comentario->id])}}">
                                    @csrf
                                    @method('DELETE')
                                    <input class="btn btn-danger mb-2" type="submit" value="Borrar">
                                </form>
                            </td>
                        @endif
                    </tr>
                @endif
            </table>
        @endforeach
    @else
        <table class="container">
            <tr>
                <td class="text-center">
                    {{auth()->user()->name}}, todavía no has comentado ninguna vez, hazlo ahora!
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/" class="btn btn-block btn-success">Elige una canción para comentar</a>
                </td>
            </tr>
        </table>
    @endif

@endsection