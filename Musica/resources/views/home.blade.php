@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Acciones</div>

                <div class="card-body">
                    <a href="" class="btn btn-success">Tus canciones</a>
                    <a href="/comentarios/index" class="btn btn-primary">Tus comentarios</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
