<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MusicaController;
use App\Models\Musica;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ComentarioController;

//TODO:Cambiar el nombre de usuario de la base de datos de laravel a root
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/',function()
{
    return redirect('/musica/index');
});

Route::prefix('musica')->group(function(){
    Route::get('/index',[MusicaController::class,'all'])->name('musica.index')->withoutMiddleware(App\Http\Middleware\Authenticate::class);
    Route::get('/create',[MusicaController::class,'create'])->name('musica.create');
    Route::post('/store',[MusicaController::class,'store'])->name('musica.store');
    Route::get('/show/{id}',[MusicaController::class,'show'])->name('musica.show')->withoutMiddleware(App\Http\Middleware\Authenticate::class);
    Route::delete('destroy/{id}',[MusicaController::class,'destroy'])->name('musica.destroy');
});

Route::prefix('comentarios')->group(function(){
    Route::post('/store',[ComentarioController::class,'store'])->name('comentario.store');
    Route::get('/index',[ComentarioController::class,'all'])->name('comentarios.index');
    Route::delete('destroy/{id}',[ComentarioController::class,'destroy'])->name('comentarios.destroy');
});
