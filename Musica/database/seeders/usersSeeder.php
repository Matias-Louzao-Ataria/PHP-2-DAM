<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(
            [
                'name'=>'admin',
                'role'=>0,
                'email'=>'a@a.com',
                'password'=>bcrypt('123')
            ]
        );
    }
}
