<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;
use App\Models\Musica;

class ComentarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');    
    }

    public function store(Request $request)
    {
        $musica_id = $request->musica_id;
        $musica = Musica::findOrFail($musica_id);

        $data = $request->validate([
            'content'=>'required'
        ]);

        $comentario = new Comentario();

        $comentario->fill($data);
        $comentario->likes = 0;
        $comentario->user_id = auth()->user()->id;
        $comentario->musica_id = $musica_id;
        $comentario->save();
        return back()->with(['music'=>$musica,'comentarios'=> Comentario::all()]);
    }

    public function all()
    {
        return view('comentarios.all')->with(['comentarios'=>Comentario::all()]);
    }

    public function destroy($id)
    {
        $comentario = Comentario::findOrFail($id);
        $comentario->delete();
        return back();

    }
}
