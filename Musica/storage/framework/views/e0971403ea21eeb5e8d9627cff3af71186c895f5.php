<?php $__env->startSection('content'); ?>
<table class="container">
    <tr>
        <td>
            <h2 class="font-weight-bold">Nombre:</h2> <?php echo e($music->title); ?>

        </td>
        <?php if(!Auth::guest() && ($music->user_id == auth()->user()->id || auth()->user()->role === 0)): ?>
            <td>
                <a class="btn btn-primary btn-block">Editar</a>
            </td>
        <?php endif; ?>
    </tr>
    <tr>
        <td class="text.wrap">
            <h2 class="font-weight-bold">Información:</h2> <?php echo e($music->info); ?>

        </td>
        <?php if(!Auth::guest() && ($music->user_id == auth()->user()->id || auth()->user()->role === 0)): ?>
            <td align="right">
                <form method="POST" action="<?php echo e(route('musica.destroy',['id'=>$music->id])); ?>">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('DELETE'); ?>
                    <input class="btn btn-block btn-danger" type="submit" value="Borrar">
                </form>
            </td>
        <?php endif; ?>
    </tr>
    <?php if(isset($music->url) && strlen($music->url)): ?>
    <tr>
        <td colspan="2">
        <h2 class="font-weight-bold">URL:</h2><a href="<?php echo e($music->url); ?>"><?php echo e($music->url); ?></a>
        </td>
    </tr>
    <?php endif; ?>
    <tr>
        <td colspan="2">
            <form action="<?php echo e(route('comentario.store')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <label class="mt-5" for="content">Comenta algo!</label>
                <input class="form-control" type="text" name="content">
                <input name="musica_id" type="hidden" value="<?php echo e($music->id); ?>">
                <input class="mt-3 btn btn-block btn-primary" type="submit" value="Comentar">
            </form>
        </td>
    </tr>
    <?php if(isset($comentarios) && count($comentarios) > 0): ?>
        <tr>
            <td colspan="2">
                <div class="mt-2">
                    Comentarios
                </div>
            </td>
        </tr>
        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comentario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($comentario->musica_id === $music->id): ?>
                <tr>
                    <td class="content">
                        <div class="row">
                            <div class="col">
                                <?php echo e(App\Models\User::findOrFail($comentario->user_id)->name); ?>:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h1 class="text.wrap"><?php echo e($comentario->content); ?></h1>
                            </div>
                        </div>
                    </td>
                    <?php if(!Auth::guest() && (auth()->user()->role == 0 || auth()->user()->id == $comentario->user_id)): ?>
                            <td align="right">
                                <form method="POST" action="<?php echo e(route('comentarios.destroy',['id'=>$comentario->id])); ?>">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('DELETE'); ?>
                                    <input class="btn btn-danger mb-2" type="submit" value="Borrar">
                                </form>
                            </td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/matias/Documentos/PHP/Musica/resources/views/musica/single.blade.php ENDPATH**/ ?>