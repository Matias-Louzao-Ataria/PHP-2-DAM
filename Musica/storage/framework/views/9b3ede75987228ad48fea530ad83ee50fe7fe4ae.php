<?php $__env->startSection('content'); ?>
    <div class="container">
        <div>
            <h1>
                Añade una nueva canción!
            </h1>
        </div>
        <form action="<?php echo e(route('musica.store')); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <label for="title">Nombre</label>
                <input type="text" name="title" class="form-control" value="<?php echo e(old('title')); ?>">
                <?php if($errors->has('title')): ?>
                    <div class="text-danger"><?php echo e($errors->first('title')); ?></div>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label for="info">Información extra</label>
                <input type="text" name="info" class="form-control" value="<?php echo e(old('info')); ?>">
                <?php if($errors->has('info')): ?>
                    <div class="text-danger"><?php echo e($errors->first('info')); ?></div>
                <?php endif; ?>
            </div>
            
            <div class="form-group">
                <label for="url">URL</label>
                <input type="text" name="url" class="form-control" value="<?php echo e(old('url')); ?>">
                <?php if($errors->has('url')): ?>
                    <div class="text-danger"><?php echo e($errors->first('url')); ?></div>
                <?php endif; ?>
            </div>
            <input type="submit" class="btn btn-block btn-success" value="Añadir">
        </form>
        <?php if(isset($success)): ?>
            <div class="alert alert-primary mt-3" role="alert">
                Guardado con éxito!
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/matias/Documentos/PHP/Musica/resources/views/musica/create.blade.php ENDPATH**/ ?>