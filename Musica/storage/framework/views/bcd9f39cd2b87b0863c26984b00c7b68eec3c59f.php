<?php $__env->startSection('content'); ?>

    <?php if(isset($comentarios) && count($comentarios) > 0): ?>
        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comentario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <table class="container">
                <?php if($comentario->user_id === auth()->user()->id): ?>
                    <tr>
                        <?php if(auth()->user()->role == 0 && $comentario->user_id != auth()->user()->id): ?>
                            <td>
                                <div class="row">
                                <div class="col">
                                    de  <?php echo e(App\Models\User::findOrFail($comentario->user_id)->name); ?>:
                                </div>
                            </td>
                        <?php endif; ?>
                        <td class="content">
                            <div class="row">
                                <div class="col">
                                    en  <?php echo e(App\Models\Musica::findOrFail($comentario->musica_id)->title); ?>:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mb-3">
                                    <h1 class="text.wrap"><?php echo e($comentario->content); ?></h1>
                                </div>
                            </div>
                        </td>
                        <?php if(!Auth::guest() && (auth()->user()->role == 0 || auth()->user()->id == $comentario->user_id)): ?>
                            <td align="right">
                                <form method="POST" action="<?php echo e(route('comentarios.destroy',['id'=>$comentario->id])); ?>">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('DELETE'); ?>
                                    <input class="btn btn-danger mb-2" type="submit" value="Borrar">
                                </form>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endif; ?>
            </table>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <table class="container">
            <tr>
                <td class="text-center">
                    <?php echo e(auth()->user()->name); ?>, todavía no has comentado ninguna vez, hazlo ahora!
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/" class="btn btn-block btn-success">Elige una canción para comentar</a>
                </td>
            </tr>
        </table>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/matias/Documentos/PHP/Musica/resources/views/comentarios/all.blade.php ENDPATH**/ ?>