<?php $__env->startSection('content'); ?>
    <?php if(count($musica) > 0): ?>
        <div class="container">
            <table class="table table-borderless table-hover">
                <thead>
                    <tr>
                        <th colspan="2">
                            <a href="/musica/create" class="btn btn-block btn-success mb-3">Añade una canción!</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $musica; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>
                                Nombre: <?php echo e($music->title); ?>

                            </td>
                            <td>
                                <a class="btn btn-primary btn-block" href="<?php echo e(route('musica.show',['id'=>$music->id])); ?>">Mostrar</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text.wrap">
                                Información: <?php echo e($music->info); ?>

                            </td>
                        </tr>
                        <?php if(isset($music->url) && strlen($music->url)): ?>
                            <tr>
                                <td colspan="2">
                                    <a href="<?php echo e($music->url); ?>"><?php echo e($music->url); ?></a>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <div class="container">
            <div class="text-center">
                No hay canciones que mostrar!
            </div>
            <div>
                <a href="<?php echo e(route('musica.create')); ?>" class="btn btn-block btn-success mb-3">Añade una canción!</a>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/matias/Documentos/PHP/Musica/resources/views/musica/all.blade.php ENDPATH**/ ?>